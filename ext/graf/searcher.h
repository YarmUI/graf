#pragma once
#include <cstdint>
#include <vector>
#include <map>
#include <deque>
#include "graph.h"

class Searcher {
  public:
    const uint32_t start;
    const uint32_t end;
  private:
    std::map<uint32_t, uint32_t> forward_distance_map;
    std::map<uint32_t, uint32_t> backward_distance_map;
    std::deque<uint32_t> forward_dq;
    std::deque<uint32_t> backward_dq;
    std::vector<uint32_t> intersections;
    page_vector_type const& pages;
    uint_vector_type const& forward_links;
    uint_vector_type const& backward_links;
    uint32_t current_forward_distance;
    uint32_t current_backward_distance;
    bool ignore_redirect;
    bool ignore_date;
    bool ignore_index;
    bool ignore_nsfw;
    void forward_mapping(std::vector<uint32_t>& found_pages);
    void backward_mapping(std::vector<uint32_t>& found_pages);
    void find_intersection(std::vector<uint32_t>& found_pages);// intersecion gver化、found_pages vector化
    void find_links();
    void find_route();
    void merge_distance_map();
    bool ignore_page(uint32_t) const;

  public:
    std::map<uint32_t, uint32_t> distance_map;
    std::vector<std::pair<uint32_t, uint32_t> > links;
    std::vector<uint32_t> route;
    Searcher(const Graph& graph, uint32_t start, uint32_t end);
    void exec();
};
