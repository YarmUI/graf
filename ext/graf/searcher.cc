#include "searcher.h"
#include <iostream>

Searcher::Searcher(const Graph& graph, uint32_t start, uint32_t end) :start(start), end(end), pages(graph.pages), forward_links(graph.forward_links), backward_links(graph.backward_links), current_forward_distance(0), current_backward_distance(0), ignore_redirect(false), ignore_date(true), ignore_index(true), ignore_nsfw(true) {
  uint32_t s = start, e = end;
  if(pages[start].is_redirect) {
    s = forward_links[pages[start].forward_link_start];
  }
  if(pages[end].is_redirect) {
    e = forward_links[pages[end].forward_link_start];
  }
  forward_dq.push_back(s);
  forward_distance_map.insert(std::make_pair(s, 0));
  backward_dq.push_back(e);
  backward_distance_map.insert(std::make_pair(e, 0));
}

void Searcher::forward_mapping(std::vector<uint32_t>& found_pages) {
  while(!forward_dq.empty()) {
    const uint32_t p = forward_dq.front();
    const uint32_t d = forward_distance_map.find(p)->second;
    if(d != current_forward_distance) {
      current_forward_distance = d;
      break;
    }
    forward_dq.pop_front();
    for(uint32_t i = pages[p].forward_link_start; i < pages[p].forward_link_end; i++) {
      const uint32_t link = forward_links[i];
      if(forward_distance_map.find(link) != forward_distance_map.end()) {
        continue;
      }
      if(ignore_page(link)) { continue; }
      if(p == link) { continue; }
      if(pages[p].is_redirect) {
        forward_dq.push_front(link);
        forward_distance_map.insert(std::make_pair(link, d));
        found_pages.push_back(link);
      } else {
        forward_dq.push_back(link);
        forward_distance_map.insert(std::make_pair(link, d + 1));
        found_pages.push_back(link);
      }
    }
  }
}

void Searcher::backward_mapping(std::vector<uint32_t>& found_pages) {
  while(!backward_dq.empty()) {
    const uint32_t p = backward_dq.front();
    const uint32_t d = backward_distance_map.find(p)->second;
    if(d != current_backward_distance) {
      current_backward_distance = d;
      break;
    }
    backward_dq.pop_front();
    for(uint32_t i = pages[p].backward_link_start; i < pages[p].backward_link_end; i++) {
      const uint32_t link = backward_links[i];
      if(backward_distance_map.find(link) != backward_distance_map.end()) { continue; }
      if(ignore_page(link)) { continue; }
      if(p == link) { continue; }
      if(pages[link].is_redirect) {
        backward_dq.push_front(link);
        backward_distance_map.insert(std::make_pair(link, d));
        found_pages.push_back(link);
      } else {
        backward_dq.push_back(link);
        backward_distance_map.insert(std::make_pair(link, d + 1));
        found_pages.push_back(link);
      }
    }
  }
}

void Searcher::find_intersection(std::vector<uint32_t>& found_pages) {
  std::multimap<uint32_t, uint32_t> its;
  for(auto it : found_pages) {
    auto f_it = forward_distance_map.find(it);
    auto b_it = backward_distance_map.find(it);
    if(f_it != forward_distance_map.end() && b_it != backward_distance_map.end()) {
      its.insert(std::make_pair(it, f_it->second + b_it->second));
    }
  }
  if(its.size() == 0) { return; }
  uint32_t d = its.begin()->second;
  for(auto it : its) {
    if(it.second != d) { break; }
    intersections.push_back(it.first);
  }
}

void Searcher::merge_distance_map() {
  std::deque<uint32_t> dq;
  for(auto it : intersections) {
    dq.push_back(it);
    distance_map.insert(std::make_pair(it, forward_distance_map.find(it)->second));
  }

  while(!dq.empty()) {
    uint32_t p = dq.front();
    dq.pop_front();
    uint32_t d = forward_distance_map.find(p)->second;
    for(uint32_t i = pages[p].backward_link_start; i < pages[p].backward_link_end; i++) {
      const uint32_t link = backward_links[i];
      if(distance_map.find(link) != distance_map.end()) { continue; }
      auto it = forward_distance_map.find(link);
      if(it == forward_distance_map.end()) { continue; }
      if(pages[link].is_redirect && it->second == d) {
        dq.push_front(link);
        distance_map.insert(std::make_pair(link, it->second));
      } else if(!pages[link].is_redirect && it->second == d - 1) {
        dq.push_back(link);
        distance_map.insert(std::make_pair(link, it->second));
      }
    }
  }

  for(auto it : intersections) {
    dq.push_back(it);
  }

  while(!dq.empty()) {
    uint32_t p = dq.front();
    dq.pop_front();
    uint32_t d = distance_map.find(p)->second;
    uint32_t from_d = backward_distance_map.find(p)->second;
    for(uint32_t i = pages[p].forward_link_start; i < pages[p].forward_link_end; i++) {
      const uint32_t link = forward_links[i];
      if(distance_map.find(link) != distance_map.end()) { continue; }
      auto it = backward_distance_map.find(link);
      if(it == backward_distance_map.end()) { continue; }
      if(pages[p].is_redirect && from_d == it->second) {
        dq.push_front(link);
        distance_map.insert(std::make_pair(link, d));
      } else if(!pages[p].is_redirect && it->second == from_d - 1) {
        dq.push_back(link);
        distance_map.insert(std::make_pair(link, d + 1));
      }
    }
  }

  if(pages[start].is_redirect) {
    distance_map.insert(std::make_pair(start, 0));
  }

  if(pages[end].is_redirect) {
    uint32_t e = forward_links[pages[end].forward_link_start];
    distance_map.insert(std::make_pair(end, distance_map.find(e)->second));
  }
}

void Searcher::find_links() {
  for(auto it : distance_map) {
    auto d = it.second;
    for(uint32_t i = pages[it.first].forward_link_start; i < pages[it.first].forward_link_end; i++) {
      auto link = forward_links[i];
      auto it2 = distance_map.find(link);
      if(it2 == distance_map.end()) { continue; }
      if(it.first == it2->first) { continue; }
      if(pages[it.first].is_redirect && it2->second == d) {
        links.push_back(std::make_pair(it.first, link));
      } else if(!pages[it.first].is_redirect && it2->second == d + 1) {
        links.push_back(std::make_pair(it.first, link));
      }
    }
  }
}

void Searcher::find_route() {
  uint32_t s = start;
  uint32_t e = end;
  route.push_back(start);
  if(pages[start].is_redirect) {
    s = forward_links[pages[start].forward_link_start];
    route.push_back(s);
  }
  if(pages[end].is_redirect) {
    e = forward_links[pages[end].forward_link_start];
  }

  while(s != e) {
    uint32_t d = distance_map.find(s)->second;
    for(uint32_t i = pages[s].forward_link_start; i < pages[s].forward_link_end; i++) {
      uint32_t link = forward_links[i];
      auto it = distance_map.find(link);
      if(it == distance_map.end()) { continue; }
      if((!pages[s].is_redirect && it->second == d + 1) || (pages[s].is_redirect && it->second == d)) {
        s = link;
        route.push_back(s);
        break;
      }
    }
  }

  if(end != e) {
    route.push_back(end);
  }
}

bool Searcher::ignore_page(uint32_t n) const {
  return (ignore_redirect && pages[n].is_redirect) ||
    (ignore_date && pages[n].is_date) ||
    (ignore_index && pages[n].is_index) ||
    (ignore_nsfw && pages[n].is_nsfw);
}

void Searcher::exec() {
  while(!forward_dq.empty() && !backward_dq.empty() && intersections.size() == 0) {
    std::vector<uint32_t> found_pages;
    if(forward_dq.size() <= backward_dq.size()) {
      forward_mapping(found_pages);
    } else {
      backward_mapping(found_pages);
    }
    find_intersection(found_pages);
  }
  if(intersections.size() == 0) { return; }
  merge_distance_map();
  find_links();
  find_route();
}
