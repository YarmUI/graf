require 'mkmf'

have_library('stdc++')

$CPPFLAGS += ' -O3 -std=c++11 '
create_makefile('graf/graf')
