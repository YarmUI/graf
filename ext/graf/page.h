#include<cstdint>

struct Page {
  bool is_redirect;
  bool is_date;
  bool is_index;
  bool is_nsfw;
  uint32_t forward_link_start;
  uint32_t forward_link_end;
  uint32_t backward_link_start;
  uint32_t backward_link_end;

  Page(const bool is_redirect = false, const bool is_date = false, const bool is_index = false, const bool is_nsfw = false)
    : is_redirect(is_redirect),
      is_date(is_date),
      is_index(is_index),
      is_nsfw(is_nsfw),
      forward_link_start(0),
      forward_link_end(0),
      backward_link_start(0),
      backward_link_end(0) { }
};
