#include "graph.h"

Graph::Graph(const char* fn) : mfile(ipc::open_only, fn),
  pages(*mfile.find<page_vector_type>("pages").first),
  forward_links(*mfile.find<uint_vector_type>("forward_links").first),
  backward_links(*mfile.find<uint_vector_type>("backward_links").first) { }

Graph::~Graph() { }
