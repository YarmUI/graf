#include <new>
#include <ruby.h>
#include "graph.h"
#include "searcher.h"

static void wrap_Graph_free(Graph* p) {
  p->~Graph();
  ruby_xfree(p);
}

static VALUE wrap_Graph_alloc(VALUE self) {
  void* p = ruby_xmalloc(sizeof(Graph));
  return Data_Wrap_Struct(self, NULL, wrap_Graph_free, p);
}

VALUE wrap_Graph_initialize(VALUE self, VALUE val_fn) {
  Graph* p;
  Data_Get_Struct(self, Graph, p);
  new (p) Graph(StringValuePtr(val_fn));
  return Qnil;
}

VALUE wrap_Graph_routing(VALUE self, VALUE start, VALUE end) {
  Graph* p;
  Data_Get_Struct(self, Graph, p);
  uint32_t s = FIX2UINT(start);
  uint32_t e = FIX2UINT(end);
  Searcher searcher(*p, s, e);
  searcher.exec();

  VALUE route = rb_ary_new();
  for(auto it : searcher.route) {
    rb_ary_push(route, UINT2NUM(it));
  }

  VALUE distance_map = rb_ary_new();
  for(auto it : searcher.distance_map) {
    VALUE pair = rb_ary_new();
    rb_ary_push(pair, UINT2NUM(it.first));
    rb_ary_push(pair, UINT2NUM(it.second));
    rb_ary_push(distance_map, pair);
  }
  VALUE links = rb_ary_new();
  for(auto it : searcher.links) {
    VALUE pair = rb_ary_new();
    rb_ary_push(pair, UINT2NUM(it.first));
    rb_ary_push(pair, UINT2NUM(it.second));
    rb_ary_push(links, pair);
  }

  VALUE res = rb_hash_new();
  rb_hash_aset(res, rb_str_new2("route"), route);
  rb_hash_aset(res, rb_str_new2("distance_map"), distance_map);
  rb_hash_aset(res, rb_str_new2("links"), links);
  return res;
}

extern "C" void Init_graf() {
  VALUE c = rb_define_class("Graf", rb_cObject);

  rb_define_alloc_func(c, wrap_Graph_alloc);
  rb_define_private_method(c, "initialize", RUBY_METHOD_FUNC(wrap_Graph_initialize), 1);
  rb_define_method(c, "routing", RUBY_METHOD_FUNC(wrap_Graph_routing), 2);
}
