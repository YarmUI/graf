#pragma once

#include <boost/interprocess/managed_mapped_file.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include "page.h"

namespace ipc = boost::interprocess;
using page_vector_type = ipc::vector<Page, ipc::allocator<Page, ipc::managed_mapped_file::segment_manager> >;
using uint_vector_type = ipc::vector<uint32_t, ipc::allocator<uint32_t, ipc::managed_mapped_file::segment_manager> >;

class Graph {
  private:
    ipc::managed_mapped_file mfile;

  public:
    page_vector_type const& pages;
    uint_vector_type const& forward_links;
    uint_vector_type const& backward_links;
    Graph(const char* fn);
    virtual ~Graph();
};
